package timosoft.demoapp;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class DemoController {

    @GetMapping(path = "/")
    public ResponseType get() {
        log.info("get called");
        return new ResponseType("123");
    }

    private class ResponseType {
        String id;

        public ResponseType(String s) {
            id = s;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return "ResponseType{" +
                    "id='" + id + '\'' +
                    '}';
        }
    }
}
